import 'music.dart';
import 'dart:async';
import 'package:audioplayer/audioplayer.dart';

enum ActionMusic{
  PLAY,
  PAUSE,
  REWIND,
  FORWARD
}

enum PlayerState{
  PLAYING,
  STOPPED,
  PAUSED
}
List<Music> musicList = [
    new Music('era-caccinni_and_redemption_and_ave_maria','Era','https://firebasestorage.googleapis.com/v0/b/yendi-eac51.appspot.com/o/Era%20-%20Era%20Classics%20(2009)%2F00-era-era_classics-2009-front.jpg?alt=media&token=a2ffeb9e-d6a0-4535-b906-61ad48b5ca6f','https://firebasestorage.googleapis.com/v0/b/yendi-eac51.appspot.com/o/01-era-caccinni_and_redemption_and_ave_maria.mp3?alt=media&token=064c79d7-3091-448e-9444-7f2e978af4e7'),
    new Music('Nwaar Is The New Black','Damso','https://firebasestorage.googleapis.com/v0/b/yendi-eac51.appspot.com/o/Damso%2FCover.jpg?alt=media&token=064c3820-718a-4880-8b19-95678998234e','https://firebasestorage.googleapis.com/v0/b/yendi-eac51.appspot.com/o/Damso%2F01%20%CE%91.%20Nwaar%20Is%20The%20New%20Black%201.mp3?alt=media&token=d25ec214-22a2-44c2-b23a-a03d1b56746d'),
   
  ];

AudioPlayer audioPlayer;
StreamSubscription positionSubscription;
StreamSubscription stateSubscription;

Music actualMusic;
Duration position = new Duration(seconds:0);
Duration dure =  new Duration(seconds:30);
PlayerState statut = PlayerState.STOPPED;
int index=0;
bool mute = false;
int maxVol =0,currentVol=0;
