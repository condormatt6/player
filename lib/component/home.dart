import 'dart:ui';

import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:player/tabs/album.dart';
import 'package:player/tabs/favories.dart';
import 'package:player/tabs/list.dart';
import 'package:player/tabs/player.dart';
import 'package:player/tabs/playlist.dart';
import 'package:player/tabs/recent.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  int _currentIndex = 3;
   final tabs = [
    PlayList(),
    ListSong(),
    PlayerSong(),
    Recent(),
    Albums(),
    Favorites()
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:tabs[_currentIndex],
            bottomNavigationBar: CurvedNavigationBar(
            color:Colors.white,
            backgroundColor:Color(0xFF0A0E21),
            buttonBackgroundColor:Colors.white,
            height:50.0,
            items:<Widget>[
            Icon(Icons.home,size: 20,color: Colors.black,),
            Icon(Icons.list,size: 20,color: Colors.black,),
            Icon(Icons.music_note,size: 20,color: Colors.black,),
            Icon(Icons.refresh,size: 20,color: Colors.black,),
            Icon(Icons.album,size: 20,color: Colors.black,),
            Icon(Icons.favorite,size: 20,color: Colors.black,),

          ],
          animationDuration:Duration(
            milliseconds:200,
          ),
          animationCurve:Curves.bounceOut,
          index:_currentIndex,
         
          onTap:(index){
              debugPrint('Current index is $index');
              setState(() {
                _currentIndex=index;
              });
          },
          ),
          
    );
  }
}
