class Music {
  String titre;
  String artist;
  String musicUrl;
  String imagePath;
  Music(String titre, String artist, String musicUrl, String imagePath) {
    this.titre = titre;
    this.artist = artist;
    this.musicUrl = musicUrl;
    this.imagePath = imagePath;
  }
}
