import 'package:flutter/material.dart';
import 'package:player/screen/onboarding_screen.dart';


void main() => runApp(Player());

class Player extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData.dark().copyWith(
         primaryColor: Color(0xFF0A0E21),
         scaffoldBackgroundColor: Color(0xFF0A0E21),
      ),
      debugShowCheckedModeBanner: false,
      title: 'Player',
      home: Onboarding(),
    );
  }
}
