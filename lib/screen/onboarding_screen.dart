import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:player/main.dart';
import 'package:player/screen/login.dart';

class Onboarding extends StatefulWidget {
  @override
  _OnboardingState createState() => _OnboardingState();
}

class _OnboardingState extends State<Onboarding> {
  final int _numPages = 3;
  final PageController _pageController = PageController(initialPage: 0);
  int _currentPage = 0;
  List<Widget> _buildPageIndicator() {
    List<Widget> list = [];
    for (int i = 0; i < _numPages; i++) {
      list.add(i == _currentPage ? _indicator(true) : _indicator(false));
    }
    return list;
  }

  Widget _indicator(bool isActive) {
    return AnimatedContainer(
      duration: Duration(milliseconds: 150),
      margin: EdgeInsets.symmetric(horizontal: 4.0),
      height: 6.0,
      width: isActive ? 24.0 : 16.0,
      decoration: BoxDecoration(
        color: isActive ? Colors.white : Color(0xFF7B51D3),
        borderRadius: BorderRadius.all(Radius.circular(12)),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    /*SystemChrome.setPreferredOrientations(
        [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);*/
   
    return Scaffold(
      
      body: AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.light,
        child: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                stops: [
                  0.1,
                  0.4,
                  0.7,
                  0.9
                ],
                colors: [
                  Color(0xFF3594DD),
                  Color(0xFF4563DB),
                  Color(0xFF5036D5),
                  Color(0xFF5B16D0),
                ]),
          ),
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: 15.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Container(
                  alignment: Alignment.centerRight,
                  child: FlatButton(
                    onPressed: () {
                      setState(() {
                        Navigator.pushReplacement(context,
                            MaterialPageRoute(builder: (context) => LoginPage()));
                      });
                    },
                    child: Text('Skip',
                        style: TextStyle(
                            color: Colors.amberAccent, fontSize: 17.0)),
                  ),
                ),
                Container(
                  height: MediaQuery.of(context).size.height / 1.3,
                  child: PageView(
                    physics: ClampingScrollPhysics(),
                    controller: _pageController,
                    onPageChanged: (int page) {
                      setState(() {
                        _currentPage = page;
                      });
                    },
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.fromLTRB(35.0, 0.0, 35.0, 0.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Center(
                              child: Image(
                                image: AssetImage('images/aurora.png'),
                                height: 300.0,
                                width: 300.0,
                              ),
                            ),
                            SizedBox(height: 15.0),
                            Text(
                              'Chez-Soi \nVotre agence Immobilière',
                              style: TextStyle(
                                color: Colors.amberAccent,
                                fontSize: 18.0,
                              ),
                            ),
                            SizedBox(height: 15.0),
                            Text('Nous vous réservons votre meilleur chez-soi'),
                          ],
                        ),
                      ),
                      Padding(
                        //padding: EdgeInsets.all(40.0),
                        padding: EdgeInsets.fromLTRB(35.0, 0.0, 35.0, 0.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Center(
                              child: Image(
                                image: AssetImage('images/aurora.png'),
                                height: 300.0,
                                width: 300.0,
                              ),
                            ),
                            SizedBox(height: 15.0),
                            Text(
                              'vous avez besoin de louer\n faites le pas vers nous',
                              style: TextStyle(
                                color: Colors.amberAccent,
                                fontSize: 18.0,
                              ),
                            ),
                            SizedBox(height: 15.0),
                            Text(
                                'Nous sommes là pour vous une location de votre convenance et puis à un prix abordable'),
                          ],
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.fromLTRB(35.0, 0.0, 35.0, 0.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Center(
                              child: Image(
                                image: AssetImage('images/aurora.png'),
                                height: 300.0,
                                width: 300.0,
                              ),
                            ),
                            SizedBox(height: 15.0),
                            Text(
                              'Etes vous un vendeur\n nos services sont à votre portée',
                              style: TextStyle(
                                color: Colors.amberAccent,
                                fontSize: 18.0,
                              ),
                            ),
                            SizedBox(height: 15.0),
                            Text(
                                'De l\'estimation du prix de vente à la recherche d\'un acquéreur,on s\'occupe de tout'),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: _buildPageIndicator(),
                ),
                _currentPage != _numPages - 1
                    ? Expanded(
                        child: Align(
                          alignment: FractionalOffset.bottomRight,
                          child: FlatButton(
                            onPressed: () {
                              _pageController.nextPage(
                                  duration: Duration(milliseconds: 500),
                                  curve: Curves.ease);
                            },
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                Text(
                                  'Next',
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 22.0),
                                ),
                                SizedBox(width: 10.0),
                                Icon(
                                  Icons.arrow_forward,
                                  color: Colors.white,
                                  size: 30.0,
                                )
                              ],
                            ),
                          ),
                        ),
                      )
                    : Text(''),
              ],
            ),
          ),
        ),
      ),
      bottomSheet: _currentPage == _numPages - 1
          ? Container(
              height: 100.0,
              width: double.infinity,
              color: Color(0xFF5036D5),
              child: GestureDetector(
                onTap: () {
                  setState(() {
                    Navigator.pushReplacement(context,
                        MaterialPageRoute(builder: (context) => LoginPage()));
                  });
                },
                child: Center(
                  child: Text(
                    'Get started',
                    style: TextStyle(
                        color: Colors.amberAccent,
                        fontSize: 20.0,
                        fontWeight: FontWeight.bold),
                  ),
                ),
              ),
            )
          : Text(''),
    );
  }
}
