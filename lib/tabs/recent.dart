import 'package:flutter/material.dart';
import 'package:player/component/musicBrain.dart';

class Recent extends StatefulWidget {
  @override
  _RecentState createState() => _RecentState();
}

class _RecentState extends State<Recent> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Recents",style: TextStyle(color:Colors.amberAccent),),),
       body: ListView.builder(
        itemCount: musicList.length,
        itemBuilder: (context, index) {
          return ListTile(
            title: Text(
              musicList[index].titre,
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            subtitle: Text(musicList[index].artist),
            leading: CircleAvatar(
              backgroundColor: Colors.grey,
              child: Image.network(musicList[index].imagePath),
            ),
            trailing: Container(
              height: 100,
              width: 100,
              child: Row(
                children: <Widget>[
                  IconButton(icon: Icon(Icons.play_circle_outline), onPressed: null),
                  IconButton(icon: Icon(Icons.file_download), onPressed: null)
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
