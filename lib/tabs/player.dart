import 'package:flutter/material.dart';
import 'package:audioplayer/audioplayer.dart';
import 'package:player/component/style.dart';
import 'package:player/component/musicBrain.dart';
import 'package:player/tabs/list.dart';
import 'package:volume/volume.dart';
import 'dart:async';

class PlayerSong extends StatefulWidget {
  @override
  _PlayerSongState createState() => _PlayerSongState();
}

class _PlayerSongState extends State<PlayerSong> {
  @override
  void initState() {
    super.initState();
    actualMusic = musicList[index];
    configAudioPlayer();
    initPlatformState();
    updateVolume();
  }

  @override
  Widget build(BuildContext context) {
    double largeur = MediaQuery.of(context).size.width;

    return Scaffold(
      appBar: AppBar(
        title: Text('Lecteur', style: TextStyle(color: kColorText)),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.queue_music),
            onPressed: () {
              Navigator.push(context, MaterialPageRoute(builder: (context)=>ListSong()));
            },
          )
        ],
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              width: 170,
              margin: EdgeInsets.only(top: 20.0),
              child: Image.network(actualMusic.imagePath),
            ),
            Container(
              margin: EdgeInsets.only(top: 20.0),
              child: Text(
                actualMusic.titre,
                textScaleFactor: 2,
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 5.0),
              child: Text(actualMusic.artist),
            ),
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  textWithStyle(fromDuration(position), 0.0),
                  textWithStyle(fromDuration(dure), 0.0)
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 10.0, right: 10.0),
              child: Slider(
                  value: position.inSeconds.toDouble(),
                  max: dure.inSeconds.toDouble(),
                  min: 0.0,
                  inactiveColor: Colors.white,
                  activeColor: Colors.amberAccent,
                  onChanged: (double d) {
                    setState(() {
                      audioPlayer.seek(d);
                    });
                  }),
            ),
            Container(
              height: largeur / 5,
              margin: EdgeInsets.only(left: 10.0, right: 10.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  IconButton(icon: Icon(Icons.fast_rewind), onPressed: rewind),
                  IconButton(
                    icon: (statut != PlayerState.PLAYING)
                        ? Icon(Icons.play_arrow)
                        : Icon(Icons.pause),
                    onPressed: (statut != PlayerState.PLAYING) ? play : pause,
                    iconSize: 50.0,
                  ),
                  IconButton(
                      icon: Icon(Icons.fast_forward), onPressed: forward),
                  IconButton(icon: Icon(Icons.favorite), onPressed: null),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  double getVolumePourcent() {
    return (currentVol / maxVol) * 100;
  }

//initialiser le volume
  Future<void> initPlatformState() async {
    await Volume.controlVolume(AudioManager.STREAM_MUSIC);
  }

//update le volume

  updateVolume() async {
    maxVol = await Volume.getMaxVol;
    currentVol = await Volume.getVol;
    setState(() {});
  }
//definir le volume

  setVol(int i) async {
    await Volume.setVol(i);
  }

//gestion des text avec style
  Text textWithStyle(String data, double scale) {
    return new Text(
      data,
      textScaleFactor: scale,
      textAlign: TextAlign.center,
      style: TextStyle(color: Colors.white, fontSize: 15.0),
    );
  }

// Gestion des bouttons
  IconButton bouton(IconData icone, double taille, ActionMusic action) {
    return new IconButton(
        icon: Icon(icone),
        iconSize: taille,
        color: Colors.white,
        onPressed: () {
          switch (action) {
            case ActionMusic.PLAY:
              play();
              break;
            case ActionMusic.PAUSE:
              pause();
              break;
            case ActionMusic.REWIND:
              rewind();
              break;
            case ActionMusic.FORWARD:
              forward();
              break;
            default:
              break;
          }
        });
  }

  void configAudioPlayer() {
    audioPlayer = AudioPlayer();
    positionSubscription = audioPlayer.onAudioPositionChanged.listen((pos) {
      setState(() {
        position = pos;
      });
      if (position >= dure) {
        position = new Duration(seconds: 0);
        //passer a la musique suivante
      }
    });
    stateSubscription = audioPlayer.onPlayerStateChanged.listen((state) {
      if (state == AudioPlayerState.PLAYING) {
        setState(() {
          dure = audioPlayer.duration;
        });
      } else if (state == AudioPlayerState.STOPPED) {
        setState(() {
          statut = PlayerState.STOPPED;
        });
      }
    }, onError: (message) {
      print(message);
      setState(() {
        statut = PlayerState.STOPPED;
        dure = new Duration(seconds: 0);
        position = new Duration(seconds: 0);
      });
    });
  }

//commande
  Future play() async {
    await audioPlayer.play(actualMusic.musicUrl);
    setState(() {
      statut = PlayerState.PLAYING;
    });
  }

  Future pause() async {
    await audioPlayer.pause();
    setState(() {
      statut = PlayerState.STOPPED;
    });
  }

  Future muted() async {
    await audioPlayer.mute(!mute);
    setState(() {
      mute = !mute;
    });
  }

//retour a la musique precedente
  void forward() {
    if (index == musicList.length - 1) {
      index = 0;
    } else {
      index++;
    }
    actualMusic = musicList[index];
    audioPlayer.stop();
    configAudioPlayer();
    play();
  }

//passer a la music suivante
  void rewind() {
    if (position > Duration(seconds: 3)) {
      audioPlayer.seek(0.0);
    } else {
      if (index == 0) {
        index = musicList.length - 1;
      } else {
        index--;
      }
      actualMusic = musicList[index];
      audioPlayer.stop();
      configAudioPlayer();
      play();
    }
  }

  String fromDuration(Duration dure) {
    return dure.toString().split('.').first;
  }
}
