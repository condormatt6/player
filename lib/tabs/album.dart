import 'package:flutter/material.dart';
import 'package:player/component/style.dart';

class Albums extends StatefulWidget {
  @override
  _AlbumsState createState() => _AlbumsState();
}

class _AlbumsState extends State<Albums> {
  List<Container> albumsList = new List();
  var al = [
    {"nom": "aurora", "image": "aurora.png"},
    {"nom": "cover", "image": "cover.jpg"},
    {"nom": "aurora", "image": "aurora.png"},
    {"nom": "cover", "image": "cover.jpg"},
    {"nom": "aurora", "image": "aurora.png"},
    {"nom": "cover", "image": "cover.jpg"},
    {"nom": "aurora", "image": "aurora.png"},
    {"nom": "cover", "image": "cover.jpg"},
    {"nom": "aurora", "image": "aurora.png"},
    {"nom": "cover", "image": "cover.jpg"},
  ];

  _buatlist() async {
    for (var i = 0; i < al.length; i++) {
      final aligne = al[i];
      final String image = aligne["image"];
      albumsList.add(
        new Container(
         
            padding: const EdgeInsets.fromLTRB(4.0, 8.0, 4.0, 8.0),
            child: Card(
              child: Column(
                children: <Widget>[
                  Image.asset(
                    "images/$image",
                    fit: BoxFit.cover,
                    height: 160.7,
                    width: 160.7,
                  ),
                  Text(
                    aligne["nom"],
                    style: TextStyle(fontSize: 18),
                  )
                ],
              ),
            ),
        ),
      );
    }
  }

  @override
  void initState() {
    _buatlist();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Albums",
          style: TextStyle(color: kColorText),
        ),
      ),
      body: GridView.count(
        crossAxisCount: 2,
        children: albumsList,
      ),
    );
  }
}
