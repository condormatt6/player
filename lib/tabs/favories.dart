import 'package:flutter/material.dart';
import 'package:player/component/musicBrain.dart';
import 'package:player/component/style.dart';

class Favorites extends StatefulWidget {
  @override
  _FavoritesState createState() => _FavoritesState();
}

class _FavoritesState extends State<Favorites> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Favorite",
          style: TextStyle(color:kColorText),
        ),
      ),
      body: ListView.builder(
        itemCount: musicList.length,
        itemBuilder: (context, index) {
          return ListTile(
            title: Text(
              musicList[index].titre,
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            subtitle: Text(musicList[index].artist),
            leading: CircleAvatar(
              backgroundColor: Colors.grey,
              child: Image.network(musicList[index].imagePath),
            ),
            trailing: IconButton(icon: Icon(Icons.favorite), onPressed: () {}),
          );
        },
      ),
    );
  }
}
