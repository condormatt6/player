import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:player/component/musicBrain.dart';

class ListSong extends StatefulWidget {
  @override
  _ListSongState createState() => _ListSongState();
}

class _ListSongState extends State<ListSong> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text(
        "Titres",
        style: TextStyle(color: Colors.amberAccent),
      )),
      body: ListView.builder(
        itemCount: musicList.length,
        itemBuilder: (context, index) {
          return ListTile(
            title: Text(
              musicList[index].titre,
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            subtitle: Text(musicList[index].artist),
            leading: CircleAvatar(
              backgroundColor: Colors.grey,
              child: Image.network(musicList[index].imagePath),
            ),
            trailing: Container(
              height: 100,
              width: 100,
              child: Row(
                children: <Widget>[
                  IconButton(
                      icon: Icon(Icons.play_circle_outline),
                      onPressed: () {
                        setState(() {
                          print("play/pause");
                        });
                      }),
                  IconButton(
                    icon: Icon(Icons.file_download),
                    onPressed: () => Fluttertoast.showToast(
                      msg: "Downloading",
                    ),
                  )
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
