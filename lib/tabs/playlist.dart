import 'package:flutter/material.dart';
class PlayList extends StatefulWidget {
  @override
  _PlayListState createState() => _PlayListState();
}

class _PlayListState extends State<PlayList> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Home',
          style: TextStyle(color: Colors.amberAccent),
        ),
      ),
      drawer: new Drawer(
        child: new ListView(
          children: <Widget>[
            new UserAccountsDrawerHeader(
              accountName: Text("Totopo"),
              accountEmail: Text("Totopo@gmail.com"),
              currentAccountPicture: GestureDetector(
                child: new CircleAvatar(
                  backgroundColor: Colors.grey,
                  child: Icon(Icons.person, color: Colors.white),
                ),
              ),
              decoration: new BoxDecoration(
                color: Color(0xFFEB1555),
              ),
            ),
            InkWell(
              child: ListTile(
                title: Text("A Ecouter"),
                leading: Icon(Icons.music_note),
                onTap: () {},
              ),
            ),
            InkWell(
              child: ListTile(
                title: Text("Récent"),
                leading: Icon(Icons.refresh),
                onTap: () {},
              ),
            ),
            InkWell(
              child: ListTile(
                title: Text("Bibliothèque"),
                leading: Icon(Icons.dashboard),
                onTap: () {},
              ),
            ),
            Divider(),
            InkWell(
              child: ListTile(
                title: Text("Parametres"),
                leading: Icon(
                  Icons.settings,
                  color: Colors.amberAccent,
                ),
                onTap: () {},
              ),
            ),
            InkWell(
              child: ListTile(
                title: Text("Aides & Commentaires"),
                leading: Icon(
                  Icons.help,
                  color: Colors.redAccent,
                ),
                onTap: () {},
              ),
            ),
          ],
        ),
      ),
      body: Container(
        child:Text("Home",style:TextStyle(color: Colors.amberAccent))
      ),
      
    );
  }
}